processhardening
=========

The role restricts the size of core dumps of and enables Randomized Virtual Memory Region Placement in standard with CIS Red Hat Enterprise Linux Benchmark

Requirements
------------

None

Role Variables
--------------

enable_processhardening: true - [controls if the module will run at all]

processhardening_restrict_core_dumps: 409600 - [restrict core dumps to 400 MiB]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev
